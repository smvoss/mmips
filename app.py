from isa import mips
from appJar import gui

app = gui()


def __main__():

    mips_cpu = mips.MIPS()

    """
    mips_cpu.display_registers()

    mips_cpu.load_program("min-max.asm")

    mips_cpu.execute()

    mips_cpu.display_registers()
    print(mips_cpu.dump_registers())
    """

    current_registers = mips_cpu.dump_registers()[1:]

    def update_mem():
        app.replaceAllTableRows("memory-table", mips_cpu.dump_memory()[1:])

    def update_regs():
        nonlocal current_registers

        diff = []
        prev = current_registers
        current_registers = mips_cpu.dump_registers()[1:]

        for idx, row in enumerate(prev):
            if prev[idx][2] != current_registers[idx][2]:
                diff.append(idx)

        app.replaceAllTableRows("register-table", current_registers)

        for idx in range(len(prev)):
            if idx == 0:
                continue  # don't select/unselect the header
            app.selectTableRow("register-table", idx, highlight=True if idx in diff else False)

    def update_gui():
        update_regs()
        update_mem()

    def local_load_program():
        code = app.getTextArea("asm-code").split("\n")
        try:
            mips_cpu.load_program(code)
        except NotImplementedError as e:
            app.startSubWindow("Error Compiling Code", modal=True)
            app.addLabel(e)
            app.stopSubWindow()
            app.showSubWindow("Error Compiling Code")

    def step():
        mips_cpu.step()
        update_gui()

    def execute():
        if not mips_cpu.program:
            local_load_program()

        try:
            mips_cpu.execute()
        except NotImplementedError as e:
            app.startSubWindow("Error Executing Code", modal=True)
            app.addLabel(e)
            app.stopSubWindow()
            app.showSubWindow("Error Executing Code")

        update_gui()

    def reset():
        pass

    # build GUI
    app.startFrame("Code", row=0, column=0)

    app.setStretch('column')
    app.startFrame("buttons", row=0, column=0, rowspan=1)
    app.addButton("Compile", local_load_program, row=0, column=0)
    app.addButton("Run", execute, row=0, column=1)
    app.addButton("Step", step, row=0, column=2)
    app.addButton("Reset", reset, row=0, column=3)
    app.stopFrame()
    app.setStretch("both")

    app.addScrolledTextArea("asm-code", row=1, column=0)
    app.stopFrame()

    app.startTabbedFrame("cpu-viewer", row=0, column=1)

    app.startTab("Registers")
    app.addTable("register-table", mips_cpu.dump_registers())

    app.stopTab()

    app.startTab("Memory Map")
    app.addTable("memory-table", mips_cpu.dump_memory())

    app.stopTab()

    app.stopTabbedFrame()

    # load a default project, the "min max" program we wrote in class
    with open("min-max.asm") as default_app:
        lines = []
        for line in default_app.readlines():
            lines += line
        app.setTextArea("asm-code", "".join(lines))

    app.go()


if __name__ == "__main__":
    __main__()
