.text
	.globl __start

__start:

lb $s1, count   # maxloops
li $s2, 0       # counter
li $s3, 100     # minimum
li $s4, 0       # maximum
li $s5, 0       # addr offset = (counter << 2)

# jump to start of loop, no increments yet
b loop

# pre loop incrementing
inc_loop:
addi $s2, $s2, 1
sll $s5, $s2, 2
beq $s1, $s2, end

loop:
lb $s0, array($s5)
blt $s0, $s3, _set_min   # if $s0 is less than $s3, found new min
try_max:
bgt $s0, $s4, _set_max   # if $s0 is greater than $s4, found new max
b inc_loop

_set_min:
add $s3, $s0, $zero      # set the min
b try_max                # check if it is also the max (ie the first number can be both)

_set_max:
add $s4, $s0, $zero
b inc_loop

end:
b end # loop forever, the program is done

.data
array: .word 3,4,2,6,12,7,18,26,2,14,19,7,8,12,13
count: .word 15
