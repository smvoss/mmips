from enum import Enum

import cpu


class MIPS(cpu.GenericCPU):

    def __init__(self):
        self.Register = self.MipsRegister
        self.opcode = {
            "lb": self.load_byte,
            "li": self.load_immediate,
            "b": self.branch,
            "addi": self.add_immediate,
            "sll": self.shift_left_logical,
            "beq": self.branch_if_equal,
            "blt": self.branch_less_than,
            "bgt": self.branch_greater_than,
            "add": self.add,
        }

        cpu.GenericCPU.__init__(self)

    class MipsRegister(Enum):
        zero = 0
        at = 1
        v0 = 2
        v1 = 3
        a0 = 4
        a1 = 5
        a2 = 6
        a3 = 7
        t0 = 8
        t1 = 9
        t2 = 10
        t3 = 11
        t4 = 12
        t5 = 13
        t6 = 14
        t7 = 15
        s0 = 16
        s1 = 17
        s2 = 18
        s3 = 19
        s4 = 20
        s5 = 21
        s6 = 22
        s7 = 23
        t8 = 24
        t9 = 25
        k0 = 26
        k1 = 27
        gp = 28
        sp = 29
        fp = 30
        ra = 31
        pc = 32
        hi = 33
        lo = 34

    def load_byte(self, wb, memaddr):
        # get value from memory address
        if '(' in memaddr:
            label, offst = memaddr.split('(')
            loc = self.memory_map[label] + self.get_register_value(offst[:-1])
        else:
            loc = self.memory_map[memaddr]

        value = self.get_memory_value(loc)

        # write memory value back into register
        self.set_register(wb, value)

    def load_immediate(self, wb, value):
        # write memory value back into register
        self.set_register(wb, value)

    def branch(self, label):
        self.set_register('pc', self.jump_table[label])

    def add_immediate(self, wb, val, imm):
        self.set_register(wb, self.get_register_value(val)+int(imm))

    def shift_left_logical(self, wb, val, shift):
        old = self.get_register_value(val)
        self.set_register(wb, old << int(shift))

    def branch_if_equal(self, v1, v2, label):
        if self.get_register_value(v1) == self.get_register_value(v2):
            self.set_register('pc', self.jump_table[label])

    def branch_less_than(self, v1, v2, label):
        if self.get_register_value(v1) < self.get_register_value(v2):
            self.set_register('pc', self.jump_table[label])

    def branch_greater_than(self, v1, v2, label):
        if self.get_register_value(v1) > self.get_register_value(v2):
            self.set_register('pc', self.jump_table[label])

    def add(self, wb, r1, r2):
        v1 = self.get_register_value(r1)
        v2 = self.get_register_value(r2)
        self.set_register(wb, v1 + v2)
