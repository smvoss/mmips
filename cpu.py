from copy import deepcopy


class GenericCPU:

    # meta-variables
    debug = False

    Register = None
    register_values = []

    memory = []
    memory_baseaddr = 0x10010000

    # program flow variables
    entrypoint = None
    program = []

    jump_table = {}
    memory_map = {}

    opcode = {}

    def __init__(self):
        self.executing = False

        # the CPU implementation calling this will assign self.Register, with an
        #  enum describing the registers of the system. Set them all to 0 before
        #  doing anything else
        for available_register in self.Register:
            self.register_values.append(0)

        # max of 48 8 bit words, initialized to 0
        for idx in range(0, 48):
            self.memory.append(0)

    def load_program(self, program):
        """
        Load a program, find text/data sections

        text:
            * remote all empty lines
            * comment lines
            * condense code
            * find labels, remove them, save off their locations

        data section: load each variable into memory, starting at memory_baseaddr


        :param program: an array of lines (ex: all lines read in from .asm file)
        :return:
        """

        # when reading in the asm, look for sections .text, .data
        current_section = None
        data_ptr = 0

        for line in program:
            # drop out all comments
            line = line.split('#')[0].strip().strip("\n")

            if not line:
                continue
            elif line == '.text' or line == '.data':
                current_section = line
                continue

            if current_section == ".text":
                if '.globl' in line:
                    self.entrypoint = line.split(" ")[1]
                elif line.endswith(':'):
                    self.jump_table[line[:-1]] = len(self.program)
                else:
                    self.program.append(line)

            elif current_section == '.data':
                data_name, size, data = line.split()

                if size != ".word":
                    raise NotImplementedError("Only values of size 'word' are supported")

                self.memory_map[data_name[:-1]] = data_ptr * 4

                for value in data.split(','):
                    self.memory[data_ptr] = value
                    data_ptr += 1

                self.dbg_print(f"data: {line}")
                self.dbg_print(f"\t  {data_name} {size} {data}")
                self.dbg_print(f"values added into memory:\n\t  {self.memory_map}")
                self.dbg_print(f"\t  {self.memory}")

        for idx, command in enumerate(self.program):
            if command.endswith(':'):
                self.jump_table[command[:-1]] = idx
                self.program.pop(idx)

        if self.debug:
            print(self.program)
            print(self.jump_table)

    def step(self):
        """
        Execute a single instruction of the loaded program

        :return: none
        """
        pc = self.pc()
        instruction = self.program[pc].replace(',', '').split(' ')

        op = instruction[0]
        args = instruction[1:]

        if op in self.opcode:
            self.opcode[op](*args)
        else:
            raise NotImplementedError(f"CPU being used has not implemented {op}!")

        if op == "b" and args[0] == "end":
            self.executing = False
        elif pc == self.pc():
            self.inc_register('pc')

    def execute(self):
        """
        Execute the currently running program, until "b end" is found

        This allows the cpu to stop execution, instead of running forever.

        :return: none
        """
        self.set_register('pc', 0)
        self.executing = True

        while self.executing:
            self.step()

    def inc_register(self, name):
        """
        Increment a register. This is an alias to set_register which just
        adds one to the current value. This is used often so it has been
        implemented.

        :param name: register to increment
        :return: none
        """
        self.register_values[self.get_register(name).value] += 1

    def set_register(self, name, value):
        """
        Update register with new value

        :param name: register to update
        :param value: new value to add
        :return: none
        """
        self.register_values[self.get_register(name).value] = int(value)

    def get_register(self, name):
        """
        Return the register enum for a register

        :param name: Reference to a register (ex $s1)
        :return: enum of register (ex Register.s1)
        """
        name = name.strip('$')  # drop the leading $ if passed in
        return self.Register[name]

    def get_register_value(self, reg):
        """
        Return the current value of a register

        :param reg: the register to lookup
        :return: value of the register 'reg'
        """
        return int(self.register_values[self.get_register(reg).value])

    def dump_registers(self):
        """
        Dump all of the registers for the CPU with their name, number and value

        :return: a 2d array of [name, number, value] for each register. Prepended
                    with a string header describing the values returned.
        """
        registers = [["Name", "Number", "Value"]]
        for enum in self.Register:
            registers.append([f"{enum.name}", enum.value,
                              self.register_values[enum.value]])
        return registers

    def display_registers(self):
        """
        Print the current state of all registers, used for debugging

        :return: none
        """
        for enum in self.Register:
            print(f"{enum}: {self.register_values[enum.value]}")

    def get_memory_value(self, loc):
        """
        Get the memory value at offset 'loc'

        :param loc: offset of memory location to retrieve
        :return: memory value at 'loc'
        """
        return self.memory[int(loc/4)]

    def dump_memory(self):
        """
        Dump all memory, with a prepended header describing the offset
        of the returned memory addresses

        :return: 2d array of memory addresses and their values
        """
        mem_dump = [["Address", "0x0", "0x4", "0x8", "0xc", "0x10", "0x14", "0x18", "0x1c"]]
        cur_offset = []
        for idx in range(int(len(self.memory)/8)):
            cur_offset.clear()
            cur_offset.append(hex(self.memory_baseaddr + (idx*0x20)))
            for i in range(8):
                cur_offset.append(self.memory[(idx*8)+i])
            mem_dump.append(deepcopy(cur_offset))

        return mem_dump

    def pc(self):
        """
        Get the current program counter

        :return: current value of register 'pc'
        """
        return self.register_values[self.get_register('pc').value]

    def dbg_print(self, *args, **kwargs):
        """
        Print debug statements to the console. This requires "enable_debug()"
        to have been previously called, otherwise prints nothing.

        :param args: passed to print
        :param kwargs: passed to print
        :return: none
        """
        if self.debug:
            print(*args, **kwargs)

    def enable_debug(self):
        """
        Enable debug mode, enabling dbg_print() to print to the console

        :return: none
        """
        self.debug = True

    def disable_debug(self):
        """
        Disable debug mode, blocking dbg_print() from printing to the console

        :return: none
        """
        self.debug = False
